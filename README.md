python -m grpc.tools.protoc -Iprotos --python_out=. --grpc_python_out=. protos/items.proto

export BIND=eth0
export CONSUL_HOST=172.17.0.2
dig @$CONSUL_HOST -p 8600 stock.service.consul. A

# Configuration stuff for .net
https://github.com/lvermeulen/Nanophone/blob/master/src/Nanophone.AspNetCore.ConfigurationProvider/Nanophone.AspNetCore.ConfigurationProvider.xproj

https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration#writing-custom-providers
https://github.com/PlayFab/consuldotnet
http://www.strathweb.com/2016/09/strongly-typed-configuration-in-asp-net-core-without-ioptionst/