using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CollectR.Models
{
    public enum State
    {
        FULFILLED,
        PENDING,
        PROVISIONED,
        CANCELED
    }

    public class PaymentRequest
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        [Required]
        [MaxLength(128)]
        public string Payee { get; set; }

        [Required]
        [Range(0, 100.0)]
        public decimal Amount { get; set; } = 0.0M;

        [JsonProperty("display_text")]
        public string DisplayText { get; set; }

        [JsonProperty("origin")]
        public string Origin { get; set; }

        [JsonProperty("state")]
        [JsonConverter(typeof(StringEnumConverter))]
        public State State { get; set; } = State.PENDING;


        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;

    }
}
