using Microsoft.EntityFrameworkCore;

namespace CollectR.Models
{
    public class PaymentContext : DbContext
    {
        public PaymentContext(DbContextOptions<PaymentContext> options) : base(options) { }
        public DbSet<PaymentRequest> Requests { get; set; }
    }
}
