using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;

using Microsoft.EntityFrameworkCore;

using CollectR.Models;

using mCash;

namespace CollectR
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile($"appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        
        public IConfigurationRoot  Configuration { get; set; }

        #region snippet_AddSingleton
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddRouting();
            services.AddOptions();
            services.Configure<mCashOptions>(Configuration.GetSection("mCash"));

            services.AddSingleton<IConfiguration>(Configuration);

            var sqlConnectionString = Configuration.GetConnectionString("DataAccessMySqlProvider");
            services.AddDbContext<PaymentContext>(options => {
                options.UseMySql(sqlConnectionString);
            });
        }
        #endregion
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory factory)
        {

            // Configure logging
            // https://docs.asp.net/en/latest/fundamentals/logging.html
            factory.AddConsole();
            factory.AddDebug();

            // Configure ASP.NET MVC
            // https://docs.asp.net/en/latest/mvc/index.html
            app.UseMvc();
        }
    }
}
