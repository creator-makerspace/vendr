using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Net.Http;
using System;


using CollectR.Models;
using Microsoft.AspNetCore.Hosting;

namespace CollectR.Controllers
{

    [Route("[controller]")]
    public class PaymentsController : Controller
    {
        public readonly IHostingEnvironment _hostingEnv;
        private readonly ILogger _logger;
        private readonly PaymentContext _context;
        private mCash.mCashOptions _mcashOptions {get; set; }

        public PaymentsController(ILoggerFactory loggerFactory, PaymentContext paymentContext, IHostingEnvironment hostingEnv, IOptions<mCash.mCashOptions> mcashOptions)
        {
            _logger = loggerFactory.CreateLogger<PaymentsController>();
            _context = paymentContext;
            _hostingEnv = hostingEnv;
            _mcashOptions = mcashOptions.Value;
        }

        [HttpGet]
        public IActionResult Payments()
        {
            var payments = new Collection<CollectR.Models.PaymentRequest>();
            payments.Add(new CollectR.Models.PaymentRequest { });
            return Json(payments);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetRequest(string id)
        {
            var request = await _context.Requests.FirstOrDefaultAsync(b => b.ID.Equals(id));

            if (request == null)
            {
                return this.NotFound(id);
            }

            return this.Ok(request);
        }

        [HttpPost]
        public async Task<IActionResult> CreateRequest([FromBody] CollectR.Models.PaymentRequest request)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            _context.Add(request);
            var result = await _context.SaveChangesAsync();
            if (result == 0)
            {
                return this.BadRequest(request);
            }

            // Place the new payment towards the provider
            HttpClient httpClient = new HttpClient();

            bool dev = _hostingEnv.IsDevelopment();
            string uri = dev ? "https://mcashtestbed.appspot.com/merchant/v1/" : "https://api.mca.sh/merchant/v1/";
            httpClient.BaseAddress = new Uri(uri);

            string authMech = "SECRET";
            string authSecret = _mcashOptions.Secret;

            httpClient.DefaultRequestHeaders.Add("Accept", "application/vnd.mcash.api.merchant.v1+json");
            httpClient.DefaultRequestHeaders.Add("X-Mcash-Merchant", _mcashOptions.Merchant);
            httpClient.DefaultRequestHeaders.Add("X-Mcash-User", _mcashOptions.User);
            httpClient.DefaultRequestHeaders.Add("X-Testbed-Token", _mcashOptions.TestbedToken);
            string secret = string.Format("{0} {1}", authMech, _mcashOptions.Secret);
            httpClient.DefaultRequestHeaders.Add("Authorization", secret);

            _logger.LogInformation(_mcashOptions.CallbackURI);
            mCash.Models.CreatePaymentRequest payment = new mCash.Models.CreatePaymentRequest();
            payment.callback_uri = _mcashOptions.CallbackURI;
            payment.amount = string.Format("{0:0.##}", request.Amount);
            payment.customer = request.Payee;
            payment.currency = "NOK";
            payment.allow_credit = false;
            payment.pos_id = request.Origin;
            payment.pos_tid = request.ID.ToString();
            payment.action = "sale";
            payment.expires_in = 600;
            payment.text = request.DisplayText;

            HttpResponseMessage response = await httpClient.PostAsJsonAsync("payment_request/", payment);

            if (!response.IsSuccessStatusCode)
            {
                _logger.LogWarning(await response.Content.ReadAsStringAsync());
                return this.BadRequest();
            }
            
            return Json(request);
        }
    }
}
