using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

using System.Threading.Tasks;

namespace CollectR.Controllers
{
    public class mCashController : Controller
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _config;

        public mCashController(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<VippsController>();
        }

        [HttpPost("cb/mcash")]
        public async Task<string> Callback()
        {
            _logger.LogInformation("Got callback from mCash");

            return "";
        }
    }
}
