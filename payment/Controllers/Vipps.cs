using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

using System.Threading.Tasks;

namespace CollectR.Controllers
{
    public class VippsController : Controller
    {
        private readonly ILogger _logger;

        public VippsController(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<VippsController>();
        }

        [HttpPost("api/cb/vipps")]
        public async Task<string> Callback()
        {
            _logger.LogInformation("Got callback from Vipps");

            return "";
        }
    }
}
