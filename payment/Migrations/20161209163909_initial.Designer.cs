﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using CollectR.Models;

namespace payment.Migrations
{
    [DbContext(typeof(PaymentContext))]
    [Migration("20161209163909_initial")]
    partial class initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752");

            modelBuilder.Entity("CollectR.Models.PaymentRequest", b =>
                {
                    b.Property<Guid>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Amount");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("Origin");

                    b.Property<string>("Payee")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<int>("State");

                    b.HasKey("ID");

                    b.ToTable("Requests");
                });
        }
    }
}
