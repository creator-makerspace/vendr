CREATE TABLE `__EFMigrationsHistory` (
    `MigrationId` varchar(95) NOT NULL,
    `ProductVersion` varchar(32) NOT NULL,
    CONSTRAINT `PK___EFMigrationsHistory` PRIMARY KEY (`MigrationId`)
);
CREATE TABLE `Requests` (
    `ID` char(36) NOT NULL,
    `Amount` decimal(65, 30) NOT NULL,
    `CreatedOn` datetime(6) NOT NULL,
    `Payee` varchar(128) NOT NULL,
    `State` int NOT NULL,
    CONSTRAINT `PK_Requests` PRIMARY KEY (`ID`)
);
INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20161207192411_initial', '1.1.0-rtm-22752');
