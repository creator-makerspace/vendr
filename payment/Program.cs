﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using System.Text;

using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;

namespace CollectR
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseKestrel()
                .UseStartup<Startup>()
                .Build();
            host.Run();
        }
    }
}
