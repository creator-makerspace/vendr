using System.Threading.Tasks;
using System;
using mCash.Models;

namespace mCash
{
    public class mCashOptions
    {
        public string User { get; set; }
        public string Merchant { get; set; }
        public string Secret {get; set;}
        public string TestbedToken { get; set; }
        public string CallbackURI {get; set; }
    }
}

