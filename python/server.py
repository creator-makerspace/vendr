from concurrent import futures
import os
import time
import logging

import consul
import grpc
import sqlalchemy as sa
from sqlalchemy.orm import exc as orm_exc

import items_pb2
import db

logging.basicConfig(level="DEBUG")
logging.getLogger('').setLevel(logging.DEBUG)
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

LOG = logging.getLogger(__name__)

_consul = None

_engine = None
_maker = None
_session = None

SVC = 'stock'


def _create_maker():
    from sqlalchemy.orm import sessionmaker

    global _engine, _maker

    con = _consul.kv.get('db/connection')[1]["Value"]

    if _engine is None:
        _engine = sa.create_engine(con)
    if _session is None:
        _maker = sessionmaker(bind=_engine)
    return _maker


def get_session():
    global _maker, _session
    if _maker is None:
        _create_maker()
    if _session is None:
        _session = _maker(autocommit=True)
    return _session


class Stock(items_pb2.StockServicer):
    def Add(self, request, context):
        s = get_session()

        item = db.Item()
        item.name = request.name
        item.description = str(request.description)
        item.quantity = request.quantity
        item.bay = request.bay

        with s.begin():
            s.add(item)
        request.id = item.id

        return request

    def Fetch(self, request, context):
        s = get_session()
        q = s.query(db.Item).filter_by(**request.filters)

        item = items_pb2.Item()

        try:
            ref = q.one()
            item.id = ref.id
            item.name = ref.name
            item.description = ref.description
        except orm_exc.NoResultFound:
            pass
        return item

    def Query(self, request, context):
        s = get_session()

        for i in s.query(db.Item):
            yield items_pb2.Item(
                name=i.name,
                description=i.description,
                quantity=i.quantity)

    def UpdateStock(self, request, context):
        s = get_session()

        q = s.query(db.Item).filter_by(name=request.name, bay=request.bay)
        item = q.one()

        item.quantity = request.quantity
        s.add(item)

        return items_pb2.StockResponse(msg="Updated", status=0)


def serve():
  global _consul
  _consul = consul.Consul(host=os.environ.get("CONSUL_HOST"))
  _consul.agent.service.register("vendr-stock", address=os.environ.get("BIND"), port=1900, service_id="foo")

  _create_maker()

  db.create(_engine)

  server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
  items_pb2.add_StockServicer_to_server(Stock(), server)
  server.add_insecure_port(os.environ.get("BIND") + ':1900')
  server.start()

  try:
    while True:
      time.sleep(_ONE_DAY_IN_SECONDS)
  except KeyboardInterrupt:
    server.stop(0)

if __name__ == '__main__':
  serve()
