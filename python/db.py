import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base


BASE = declarative_base()


class Item(BASE):
    __tablename__ = 'items'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.Unicode(20), nullable=False)
    description = sa.Column(sa.UnicodeText(), nullable=False)
    quantity = sa.Column(sa.Integer, nullable=False)
    bay = sa.Column(sa.Integer, nullable=False)


def create(engine):
    BASE.metadata.bind = engine
    BASE.metadata.drop_all()
    BASE.metadata.create_all()
