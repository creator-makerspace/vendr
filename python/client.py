from __future__ import print_function
import os
import logging

import consul
import grpc

import items_pb2

logging.basicConfig(level="DEBUG")

brand = [("Coke", "coke")]
_consul = None


STOCK = "vendr-stock"


def run():
    #import ipdb; ipdb.set_trace()
    svc = _consul.catalog.service(STOCK)[1][0]
    addr = "%s:%d" % (svc["ServiceAddress"], svc["ServicePort"])
    channel = grpc.insecure_channel(addr)
    stub = items_pb2.StockStub(channel)

    q = items_pb2.QueryParams()
    q.filters["id"] = str(1)
    item = stub.Fetch(q)

    if not item.id:
        new = items_pb2.Item(name="cc_normal", description="Normal coca cola", quantity=10, bay=1)
        stub.Add(new)

    q = items_pb2.QueryStreamParams()
    response = stub.Query(q)
    for i in response:
        print(i.quantity)

    stub.UpdateStock(items_pb2.StockRequest(name="cc_normal", bay=1, quantity=4))


if __name__ == '__main__':
    _consul = consul.Consul(host=os.environ.get("CONSUL_HOST"))
    run()
