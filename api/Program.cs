﻿using System;
using System.Threading.Tasks;
using System.Threading;
using System.Text;

using Microsoft.AspNetCore.Builder;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;

namespace Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile($"appsettings.json", optional: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .Build();
        }

        public IConfiguration Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {
            // https://docs.asp.net/en/latest/security/anti-request-forgery.html
            services.AddAntiforgery(options => options.CookieName = options.HeaderName = "X-XSRF-TOKEN");

            // Register Entity Framework database context
            // https://docs.efproject.net/en/latest/platforms/aspnetcore/new-db.html
            /*services.AddDbContext<DatabaseContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });*/

            services.AddMvcCore()
                .AddAuthorization()
                .AddViews()
                .AddRazorViewEngine()
                .AddJsonFormatters();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory factory)
        {
            // Configure logging
            // https://docs.asp.net/en/latest/fundamentals/logging.html
            factory.AddConsole(Configuration.GetSection("Logging"));
            factory.AddDebug();

            // Configure ASP.NET MVC
            // https://docs.asp.net/en/latest/mvc/index.html
            app.UseMvc();
        }
    }
    public class Program
    {
        public static void Main(string[] args)
        {
            Channel chan = new Channel("10.0.0.206:1900", ChannelCredentials.Insecure);
            var client = new Vendr.Stock.StockClient(chan);

            Item item = new Item();
            item.Bay = 1;
            item.Description = "Uber fanta";
            item.Quantity = 10;
            client.AddAsync(item).GetAwaiter();
            List(client).Wait();

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseStartup<Startup>()
                .Build();
            host.Run();
        }

        public static async Task List(Vendr.Stock.StockClient client)
        {
            try
            {
                QueryStreamParams streamParams = new QueryStreamParams();
                streamParams.Params = new QueryParams();

                using (var call = client.Query(streamParams))
                {
                    var responseStream = call.ResponseStream;
                    StringBuilder responseLog = new StringBuilder("Result: ");

                    var cts = new CancellationTokenSource();

                    while (await responseStream.MoveNext(System.Threading.CancellationToken.None))
                    {
                        Item item = responseStream.Current;
                        Console.WriteLine(item.ToString());
                    }
                }
            }
            catch (RpcException e)
            {
                Console.WriteLine("Foo");
            }
        }
    }
}
