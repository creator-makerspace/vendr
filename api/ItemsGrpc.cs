// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: items.proto
#region Designer generated code

using System;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;

namespace Vendr {
  public static class Stock
  {
    static readonly string __ServiceName = "vendr.Stock";

    static readonly Marshaller<global::Vendr.Item> __Marshaller_Item = Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Vendr.Item.Parser.ParseFrom);
    static readonly Marshaller<global::Vendr.QueryParams> __Marshaller_QueryParams = Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Vendr.QueryParams.Parser.ParseFrom);
    static readonly Marshaller<global::Vendr.QueryStreamParams> __Marshaller_QueryStreamParams = Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Vendr.QueryStreamParams.Parser.ParseFrom);
    static readonly Marshaller<global::Vendr.StockRequest> __Marshaller_StockRequest = Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Vendr.StockRequest.Parser.ParseFrom);
    static readonly Marshaller<global::Vendr.StockResponse> __Marshaller_StockResponse = Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Vendr.StockResponse.Parser.ParseFrom);

    static readonly Method<global::Vendr.Item, global::Vendr.Item> __Method_Add = new Method<global::Vendr.Item, global::Vendr.Item>(
        MethodType.Unary,
        __ServiceName,
        "Add",
        __Marshaller_Item,
        __Marshaller_Item);

    static readonly Method<global::Vendr.QueryParams, global::Vendr.Item> __Method_Fetch = new Method<global::Vendr.QueryParams, global::Vendr.Item>(
        MethodType.Unary,
        __ServiceName,
        "Fetch",
        __Marshaller_QueryParams,
        __Marshaller_Item);

    static readonly Method<global::Vendr.QueryStreamParams, global::Vendr.Item> __Method_Query = new Method<global::Vendr.QueryStreamParams, global::Vendr.Item>(
        MethodType.ServerStreaming,
        __ServiceName,
        "Query",
        __Marshaller_QueryStreamParams,
        __Marshaller_Item);

    static readonly Method<global::Vendr.StockRequest, global::Vendr.StockResponse> __Method_UpdateStock = new Method<global::Vendr.StockRequest, global::Vendr.StockResponse>(
        MethodType.Unary,
        __ServiceName,
        "UpdateStock",
        __Marshaller_StockRequest,
        __Marshaller_StockResponse);

    /// <summary>Service descriptor</summary>
    public static global::Google.Protobuf.Reflection.ServiceDescriptor Descriptor
    {
      get { return global::Vendr.ItemsReflection.Descriptor.Services[0]; }
    }

    /// <summary>Base class for server-side implementations of Stock</summary>
    public abstract class StockBase
    {
      public virtual global::System.Threading.Tasks.Task<global::Vendr.Item> Add(global::Vendr.Item request, ServerCallContext context)
      {
        throw new RpcException(new Status(StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task<global::Vendr.Item> Fetch(global::Vendr.QueryParams request, ServerCallContext context)
      {
        throw new RpcException(new Status(StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task Query(global::Vendr.QueryStreamParams request, IServerStreamWriter<global::Vendr.Item> responseStream, ServerCallContext context)
      {
        throw new RpcException(new Status(StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task<global::Vendr.StockResponse> UpdateStock(global::Vendr.StockRequest request, ServerCallContext context)
      {
        throw new RpcException(new Status(StatusCode.Unimplemented, ""));
      }

    }

    /// <summary>Client for Stock</summary>
    public class StockClient : ClientBase<StockClient>
    {
      /// <summary>Creates a new client for Stock</summary>
      /// <param name="channel">The channel to use to make remote calls.</param>
      public StockClient(Channel channel) : base(channel)
      {
      }
      /// <summary>Creates a new client for Stock that uses a custom <c>CallInvoker</c>.</summary>
      /// <param name="callInvoker">The callInvoker to use to make remote calls.</param>
      public StockClient(CallInvoker callInvoker) : base(callInvoker)
      {
      }
      /// <summary>Protected parameterless constructor to allow creation of test doubles.</summary>
      protected StockClient() : base()
      {
      }
      /// <summary>Protected constructor to allow creation of configured clients.</summary>
      /// <param name="configuration">The client configuration.</param>
      protected StockClient(ClientBaseConfiguration configuration) : base(configuration)
      {
      }

      public virtual global::Vendr.Item Add(global::Vendr.Item request, Metadata headers = null, DateTime? deadline = null, CancellationToken cancellationToken = default(CancellationToken))
      {
        return Add(request, new CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Vendr.Item Add(global::Vendr.Item request, CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_Add, null, options, request);
      }
      public virtual AsyncUnaryCall<global::Vendr.Item> AddAsync(global::Vendr.Item request, Metadata headers = null, DateTime? deadline = null, CancellationToken cancellationToken = default(CancellationToken))
      {
        return AddAsync(request, new CallOptions(headers, deadline, cancellationToken));
      }
      public virtual AsyncUnaryCall<global::Vendr.Item> AddAsync(global::Vendr.Item request, CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_Add, null, options, request);
      }
      public virtual global::Vendr.Item Fetch(global::Vendr.QueryParams request, Metadata headers = null, DateTime? deadline = null, CancellationToken cancellationToken = default(CancellationToken))
      {
        return Fetch(request, new CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Vendr.Item Fetch(global::Vendr.QueryParams request, CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_Fetch, null, options, request);
      }
      public virtual AsyncUnaryCall<global::Vendr.Item> FetchAsync(global::Vendr.QueryParams request, Metadata headers = null, DateTime? deadline = null, CancellationToken cancellationToken = default(CancellationToken))
      {
        return FetchAsync(request, new CallOptions(headers, deadline, cancellationToken));
      }
      public virtual AsyncUnaryCall<global::Vendr.Item> FetchAsync(global::Vendr.QueryParams request, CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_Fetch, null, options, request);
      }
      public virtual AsyncServerStreamingCall<global::Vendr.Item> Query(global::Vendr.QueryStreamParams request, Metadata headers = null, DateTime? deadline = null, CancellationToken cancellationToken = default(CancellationToken))
      {
        return Query(request, new CallOptions(headers, deadline, cancellationToken));
      }
      public virtual AsyncServerStreamingCall<global::Vendr.Item> Query(global::Vendr.QueryStreamParams request, CallOptions options)
      {
        return CallInvoker.AsyncServerStreamingCall(__Method_Query, null, options, request);
      }
      public virtual global::Vendr.StockResponse UpdateStock(global::Vendr.StockRequest request, Metadata headers = null, DateTime? deadline = null, CancellationToken cancellationToken = default(CancellationToken))
      {
        return UpdateStock(request, new CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Vendr.StockResponse UpdateStock(global::Vendr.StockRequest request, CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_UpdateStock, null, options, request);
      }
      public virtual AsyncUnaryCall<global::Vendr.StockResponse> UpdateStockAsync(global::Vendr.StockRequest request, Metadata headers = null, DateTime? deadline = null, CancellationToken cancellationToken = default(CancellationToken))
      {
        return UpdateStockAsync(request, new CallOptions(headers, deadline, cancellationToken));
      }
      public virtual AsyncUnaryCall<global::Vendr.StockResponse> UpdateStockAsync(global::Vendr.StockRequest request, CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_UpdateStock, null, options, request);
      }
      protected override StockClient NewInstance(ClientBaseConfiguration configuration)
      {
        return new StockClient(configuration);
      }
    }

    /// <summary>Creates service definition that can be registered with a server</summary>
    public static ServerServiceDefinition BindService(StockBase serviceImpl)
    {
      return ServerServiceDefinition.CreateBuilder()
          .AddMethod(__Method_Add, serviceImpl.Add)
          .AddMethod(__Method_Fetch, serviceImpl.Fetch)
          .AddMethod(__Method_Query, serviceImpl.Query)
          .AddMethod(__Method_UpdateStock, serviceImpl.UpdateStock).Build();
    }

  }
}
#endregion
