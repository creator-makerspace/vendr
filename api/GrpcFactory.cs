using Grpc.Core;
using System.Collections.Generic;

namespace Api
{
    public class GrpcHelper
    {
        Dictionary<string, Channel> channels;
        public GrpcHelper() {
            this.channels = new Dictionary<string, Channel>();
        }
        public Channel make(string svcName)
        {
            if (!this.channels.ContainsKey(svcName)) {

                this.channels.Add(svcName, new Channel("10.2.0.6"));
            }
            return this.channels[svcName];
        }
    }
}